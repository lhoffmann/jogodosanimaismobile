package com.jogodosanimais;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.jogodosanimaismobile.R;
import com.jogodosanimais.steps.NovoAnimal;
import com.jogodosanimais.steps.Step1;
import com.jogodosanimais.steps.Step2;
import com.jogodosanimais.utils.Utils;

public class JogoDosAnimais extends Activity {

	public static Step1 step1;
	public static Step2 step2;
	public static NovoAnimal novoAnimal;
	public static Utils utils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getInstance();
		Button ok = (Button) findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				iniciate();
			}
		});
	}

	private void getInstance() {
		utils = Utils.getInstance(this);
		novoAnimal = NovoAnimal.getInstance(this);
		step1 = Step1.getInstance(novoAnimal);
		step2 = Step2.getInstance(novoAnimal, utils);
	}

	public static void iniciate() {
		Builder builder = Utils.pergunta("O animal que você pensou vive na água?");
		builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				step1.etubarao();
				dialog.dismiss();
			}

		});

		builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (Utils.getAnimais().isEmpty()) {
					step2.eanimal("Macaco");
				} else {
					step2.eacao();
				}
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}
}
