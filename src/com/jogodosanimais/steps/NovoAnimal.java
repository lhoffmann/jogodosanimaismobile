package com.jogodosanimais.steps;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.widget.EditText;

import com.jogodosanimais.JogoDosAnimais;
import com.jogodosanimais.object.Animal;
import com.jogodosanimais.utils.Utils;

/**
 * Claase para criação de um novo animal
 * 
 * @author Leandro Hoffmann
 * 
 */

public class NovoAnimal {

	JogoDosAnimais jogoDosAnimais;

	public NovoAnimal(JogoDosAnimais jogoDosAnimais) {
		this.jogoDosAnimais = jogoDosAnimais;
	}

	public static NovoAnimal getInstance(JogoDosAnimais jogoDosAnimais) {
		return new NovoAnimal(jogoDosAnimais);
	}

	/**
	 * Metodo sem validação, para seguir o requisito que deve ter o mesmo
	 * comportamento do original
	 * 
	 * @param nome
	 */
	public void qualAnimal(final String nome) {
		final EditText input = new EditText(jogoDosAnimais);
		Builder builder = Utils.geraPergunta("Qual animal você pensou?");
		builder.setView(input);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				String animal = input.getText().toString();
				qualAcao(animal, nome);
			}
		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

	private void qualAcao(final String animal, String nome) {
		final EditText input = new EditText(jogoDosAnimais);
		Builder builder = Utils.geraPergunta("Um(a) " + animal + " ____ mas um(a) " + nome + " não.");
		builder.setView(input);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				String acao = input.getText().toString();
				Utils.getAnimais().add(new Animal(animal, acao));
			}

		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}