package com.jogodosanimais.steps;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;

import com.jogodosanimais.utils.Utils;

/**
 * Step que inicia perguntando se é Tubarão
 * 
 * @author Leandro Hoffmann
 * 
 */

public class Step1 {

	private NovoAnimal novoAnimal;

	public Step1(NovoAnimal novoAnimal) {
		this.novoAnimal = novoAnimal;
	}

	public static Step1 getInstance(NovoAnimal novoAnimal) {
		return new Step1(novoAnimal);
	}

	public void etubarao() {
		Builder builder = Utils.pergunta("O animal que você pensou é um Tubarão?");
		builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				Utils.acertei();
				dialog.dismiss();
			}

		});

		builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				novoAnimal.qualAnimal("Tubarão");
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}
}