package com.jogodosanimais.steps;

import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;

import com.jogodosanimais.object.Animal;
import com.jogodosanimais.utils.Utils;

/**
 * Step que verifica qual animal foi pensado e se ele possui tal caracteristica
 * 
 * @author Leandro Hoffmann
 * 
 */

public class Step2 {

	private NovoAnimal novoAnimal;
	private int count = 0;
	private static List<Animal> animais;

	public Step2(NovoAnimal novoAnimal, Utils utils) {
		this.novoAnimal = novoAnimal;
	}

	public static Step2 getInstance(NovoAnimal novoAnimal, Utils utils) {
		return new Step2(novoAnimal, utils);
	}

	/**
	 * Metodo que verifica se o animal possui tal caracteristica, caso termine a
	 * lista, pergunta se é Macaco
	 */
	public void eacao() {
		animais = Utils.getAnimais();
		perguntaAnimal(animais.get(count));
	}

	public void perguntaAnimal(final Animal animal) {
		Builder builder = Utils.pergunta("O animal que você pensou " + ((animal.getAcao() == null) ? "" : animal.getAcao()) + "?");
		builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				eanimal(animal.getNome());
				dialog.dismiss();
			}

		});

		builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				count += 1;
				if (count < Utils.getAnimais().size()) {
					perguntaAnimal(animais.get(count));
				} else {
					eanimal("Macaco");
					count = 0;
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Metodo que verifica qual o animal pensado
	 */
	public void eanimal(final String nome) {
		Builder builder = Utils.pergunta("O animal que você pensou é um " + ((nome == null) ? "" : nome) + "?");
		builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				Utils.acertei();
				dialog.dismiss();
			}

		});

		builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				novoAnimal.qualAnimal(nome);
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}