package com.jogodosanimais.utils;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;

import com.jogodosanimais.object.Animal;

/**
 * Classe de utilidades para o JPanel e Frame
 * 
 * @author Leandro Hoffmann
 * 
 */

public class Utils {

	private static ArrayList<Animal> animais;
	private static Context context;

	public Utils(Context context) {
		Utils.context = context;
	}

	public static Utils getInstance(Context context) {
		return new Utils(context);
	}

	/**
	 * Gera a pergunta e retorna o valor da resposta
	 * 
	 * @param pergunta
	 * @return resposta
	 */
	public static Builder pergunta(String pergunta) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Animais");
		builder.setMessage(pergunta);
		return builder;
	}

	/**
	 * Gera um popup indicando o acerto
	 */
	public static void acertei() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage("Acertei de novo!").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static Builder geraPergunta(String pergunta) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Jogo dos Animais");
		builder.setMessage(pergunta);
		return builder;
	}

	public static ArrayList<Animal> getAnimais() {
		if (animais == null) {
			animais = new ArrayList<Animal>();
		}
		return animais;
	}
}
